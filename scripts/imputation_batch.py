# import libraries
import os
import re
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from getpass import getuser

# specifying constant parameters
random_seed = 5

# upload pickled data
MR = pickle.load(open("MR_step2.pkl", 'rb'))
IR = pickle.load(open("IR_step2.pkl", 'rb'))

# Country # nominal # OneHot encoding
MR_country_dummy = pd.get_dummies(MR, columns=['country'], prefix='Country')
IR_country_dummy = pd.get_dummies(IR, columns=['country'], prefix='Country')

# Rename countries
country_list = ['Angola', 'Burundi', 'Ethiopia', 'Lesotho', 'Malawi', 'Mozambique', 'Namibia', 'Rwanda', 'Zambia', 'Zimbabwe']
i = 0
for colonne in list(MR_country_dummy.columns):
    if re.match('Country', colonne):
        MR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1
i = 0
for colonne in list(IR_country_dummy.columns):
    if re.match('Country', colonne):
        IR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1

# extraction of column names
MR_col_names = MR_country_dummy.columns.drop('hiv03')
IR_col_names = IR_country_dummy.columns.drop('hiv03')

#================================================================================================================================
# subsampling 9 countries out of 10
#================================================================================================================================

CMR_train = {}
CMR_test = {}
CIR_train = {}
CIR_test = {}
CMR_Y_train = {}
CMR_Y_test = {}
CIR_Y_train = {}
CIR_Y_test = {}

for name in list(country_list):
    CMR_train[name] = MR_country_dummy[MR_country_dummy[name]!=1].astype('float')
    CMR_test[name] = MR_country_dummy[MR_country_dummy[name]==1].astype('float')
    CIR_train[name] = IR_country_dummy[IR_country_dummy[name]!=1].astype('float')
    CIR_test[name] = IR_country_dummy[IR_country_dummy[name]==1].astype('float')
    CMR_Y_train[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]!=1].astype('float')
    CMR_Y_test[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]==1].astype('float')
    CIR_Y_train[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]!=1].astype('float')
    CIR_Y_test[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]==1].astype('float')
    CMR_train[name].drop(columns='hiv03', inplace=True)
    CMR_test[name].drop(columns='hiv03', inplace=True)
    CIR_train[name].drop(columns='hiv03', inplace=True)
    CIR_test[name].drop(columns='hiv03', inplace=True)

#================================================================================================================================
# remove country flag - impute - standardize
#================================================================================================================================

MR_X_train = {}
MR_X_test = {}
MR_Y_train = {}
MR_Y_test = {}
IR_X_train = {}
IR_X_test = {}
IR_Y_train = {}
IR_Y_test = {}

# split between train (80%) and test (20%) with stratification
for name in list(country_list):  
    MR_X_train[name], MR_X_test[name], MR_Y_train[name], MR_Y_test[name] = train_test_split(CMR_train[name], CMR_Y_train[name], test_size=0.2, stratify=CMR_Y_train[name], random_state=random_seed)
    IR_X_train[name], IR_X_test[name], IR_Y_train[name], IR_Y_test[name] = train_test_split(CIR_train[name], CIR_Y_train[name], test_size=0.2, stratify=CIR_Y_train[name], random_state=random_seed)

# multiple imputations using chained equations
n_imputations = 5
MR_X_train_comp = {}
MR_X_test_comp = {}
CMR_test_comp = {}
IR_X_train_comp = {}
IR_X_test_comp = {}
CIR_test_comp = {}
MR_X_train_imp = {}
MR_X_test_imp = {}
CMR_test_imp = {}
IR_X_train_imp = {}
IR_X_test_imp = {}
CIR_test_imp = {}

# for name in list(country_list)[:-1]:
# try with one
def imputer(name=''):
    assert name in country_list, 'ERROR: country not found.'
    MR_X_train_comp[name] = []
    MR_X_test_comp[name] = []
    CMR_test_comp[name] = []
    IR_X_train_comp[name] = []
    IR_X_test_comp[name] = []
    CIR_test_comp[name] = []
    for i in range(n_imputations):
        print(f'Current imputed country is {name}')
        print(f'Current imputation round is {i+1}/{n_imputations}')
        MR_imputer = IterativeImputer(sample_posterior=True, random_state=i, verbose=1)
        MR_X_train_comp[name].append(MR_imputer.fit_transform(MR_X_train[name]))
        MR_X_test_comp[name].append(MR_imputer.transform(MR_X_test[name]))
        CMR_test_comp[name].append(MR_imputer.transform(CMR_test[name]))
        IR_imputer = IterativeImputer(sample_posterior=True, random_state=i, verbose=1)
        IR_X_train_comp[name].append(IR_imputer.fit_transform(IR_X_train[name]))
        IR_X_test_comp[name].append(IR_imputer.transform(IR_X_test[name]))
        CIR_test_comp[name].append(IR_imputer.transform(CIR_test[name]))
   
    MR_X_train_imp[name] = pd.DataFrame(np.mean(MR_X_train_comp[name], axis=0), columns=MR_col_names)
    f = open("MR_X_train_imp_%s.pkl"%name, 'wb')
    pickle.dump(MR_X_train_imp[name], f)
    
    MR_X_test_imp[name] = pd.DataFrame(np.mean(MR_X_test_comp[name], axis=0), columns=MR_col_names)
    f = open("MR_X_test_imp_%s.pkl"%name , 'wb')
    pickle.dump(MR_X_test_imp[name] , f)
    
    CMR_test_imp[name] = pd.DataFrame(np.mean(CMR_test_comp[name], axis=0), columns=MR_col_names)
    f = open("CMR_test_imp_%s.pkl"%name  , 'wb')
    pickle.dump(CMR_test_imp[name]  , f)
    
    IR_X_train_imp[name] = pd.DataFrame(np.mean(IR_X_train_comp[name], axis=0), columns=IR_col_names)
    f = open("IR_X_train_imp_%s.pkl"%name, 'wb')
    pickle.dump(IR_X_train_imp[name], f)
    
    IR_X_test_imp[name] = pd.DataFrame(np.mean(IR_X_test_comp[name], axis=0), columns=IR_col_names)
    f = open("IR_X_test_imp_%s.pkl"%name , 'wb')
    pickle.dump(IR_X_test_imp[name] , f)

    CIR_test_imp[name] = pd.DataFrame(np.mean(CIR_test_comp[name], axis=0), columns=IR_col_names)
    f = open("CIR_test_imp_%s.pkl"%name  , 'wb')
    pickle.dump(CIR_test_imp[name]  , f)
    f.close()

# end of part 1
    
