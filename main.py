import os
import re
import pickle
import pandas as pd
import numpy as np
from IPython.core.interactiveshell import InteractiveShell
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
import matplotlib
from matplotlib import pyplot as plt
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV, GridSearchCV
from sklearn.metrics import confusion_matrix, f1_score, log_loss, roc_auc_score, recall_score, precision_score
import xgboost as xgb
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression
from pygam import LogisticGAM
from sklearn.svm import SVC
import pygam as gam
from pygam import LogisticGAM

%matplotlib ipympl

InteractiveShell.ast_node_interactivity = "all"

working_directory = '/Users/cesareborgia/unige/predicting_hiv_status/temp/'
os.chdir(working_directory)

# Load raw data
%run -i "Scripts\data_processing.py"

# Load women data
%run -i "Scripts\features_engineering_women.py"

# Load men data
%run -i "Scripts\features_engineering_men.py"

# imputation and scaling data
%run -i "Scripts\scale_impute.py"

# left out country algo
%run -i "Scripts\one_out_algorithms.py"

# run xgboost on either gender
%run -i "Scripts\all countries.py"
