# import libraries
import os
import re
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from getpass import getuser

# specifying constant parameters
random_seed = 5

usr = getuser()

# set working directory
if usr == 'cesareborgia':
    working_directory = '/Users/cesareborgia/Documents/unige/predicting_hiv_status/'
elif usr == 'vstampf':
    working_directory = '/afs/cern.ch/work/v/vstampf/uni/predicting_hiv_status/'
os.chdir(working_directory)

# upload pickled data
MR = pickle.load(open("Transformed_data/MR_step2.pkl", 'rb'))
IR = pickle.load(open("Transformed_data/IR_step2.pkl", 'rb'))

# Country # nominal # OneHot encoding
MR_country_dummy = pd.get_dummies(MR, columns=['country'], prefix='Country')
IR_country_dummy = pd.get_dummies(IR, columns=['country'], prefix='Country')

# Rename countries
country_list = ['Angola', 'Burundi', 'Ethiopia', 'Lesotho', 'Malawi', 'Mozambique', 'Namibia', 'Rwanda', 'Zambia', 'Zimbabwe']
i = 0
for colonne in list(MR_country_dummy.columns):
    if re.match('Country', colonne):
        MR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1
i = 0
for colonne in list(IR_country_dummy.columns):
    if re.match('Country', colonne):
        IR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1

# extraction of column names
MR_col_names = MR_country_dummy.columns.drop('hiv03')
IR_col_names = IR_country_dummy.columns.drop('hiv03')

CMR_train = {}
CMR_test = {}
CIR_train = {}
CIR_test = {}
CMR_Y_train = {}
CMR_Y_test = {}
CIR_Y_train = {}
CIR_Y_test = {}

for name in list(country_list):
    CMR_train[name] = MR_country_dummy[MR_country_dummy[name]!=1].astype('float')
    CMR_test[name] = MR_country_dummy[MR_country_dummy[name]==1].astype('float')
    CIR_train[name] = IR_country_dummy[IR_country_dummy[name]!=1].astype('float')
    CIR_test[name] = IR_country_dummy[IR_country_dummy[name]==1].astype('float')
    CMR_Y_train[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]!=1].astype('float')
    CMR_Y_test[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]==1].astype('float')
    CIR_Y_train[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]!=1].astype('float')
    CIR_Y_test[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]==1].astype('float')
    CMR_train[name].drop(columns='hiv03', inplace=True)
    CMR_test[name].drop(columns='hiv03', inplace=True)
    CIR_train[name].drop(columns='hiv03', inplace=True)
    CIR_test[name].drop(columns='hiv03', inplace=True)

#================================================================================================================================
# remove country flag - impute - standardize
#================================================================================================================================

MR_X_train = {}
MR_X_test = {}
MR_Y_train = {}
MR_Y_test = {}
IR_X_train = {}
IR_X_test = {}
IR_Y_train = {}
IR_Y_test = {}

# split between train (80%) and test (20%) with stratification
for name in list(country_list):  
    MR_X_train[name], MR_X_test[name], MR_Y_train[name], MR_Y_test[name] = train_test_split(CMR_train[name], CMR_Y_train[name], test_size=0.2, stratify=CMR_Y_train[name], random_state=random_seed)
    IR_X_train[name], IR_X_test[name], IR_Y_train[name], IR_Y_test[name] = train_test_split(CIR_train[name], CIR_Y_train[name], test_size=0.2, stratify=CIR_Y_train[name], random_state=random_seed)

f = open("Train_samples/MR_Y_train.pkl", 'wb')
pickle.dump(MR_Y_train, f)
f = open("Test_samples/MR_Y_test.pkl", 'wb')
pickle.dump(MR_Y_test, f)
f = open("Left_one_out_samples/CMR_Y_test.pkl", 'wb')
pickle.dump(CMR_Y_test, f)
f = open("Train_samples/IR_Y_train.pkl", 'wb')
pickle.dump(IR_Y_train, f)
f = open("Test_samples/IR_Y_test.pkl", 'wb')
pickle.dump(IR_Y_test, f)
f = open("Left_one_out_samples/CIR_Y_test.pkl", 'wb')
pickle.dump(CIR_Y_test, f)
