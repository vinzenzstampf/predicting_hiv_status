# import libraries
import os
import re
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

country_list = ['Angola', 'Burundi', 'Ethiopia', 'Lesotho', 'Malawi', 'Mozambique', 'Namibia', 'Rwanda', 'Zambia', 'Zimbabwe']

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = '/Users/cesareborgia/Documents/unige/predicting_hiv_status/'
os.chdir(working_directory)

MR_X_train_comp = {}
MR_X_test_comp = {}
CMR_test_comp = {}
IR_X_train_comp = {}
IR_X_test_comp = {}
CIR_test_comp = {}
MR_X_train_imp = {}
MR_X_test_imp = {}
CMR_test_imp = {}
IR_X_train_imp = {}
IR_X_test_imp = {}
CIR_test_imp = {}

# load data from part 1...
for name in list(country_list):
    print(f'loading {name}...')
    MR_X_train_imp[name] = pickle.load(open("Imputation_data/tmp/MR_X_train_imp_%s.pkl"%name, 'rb'))
    MR_X_test_imp[name]  = pickle.load(open("Imputation_data/tmp/MR_X_test_imp_%s.pkl"%name , 'rb'))
    CMR_test_imp[name]   = pickle.load(open("Imputation_data/tmp/CMR_test_imp_%s.pkl"%name  , 'rb'))
    IR_X_train_imp[name] = pickle.load(open("Imputation_data/tmp/IR_X_train_imp_%s.pkl"%name, 'rb'))
    IR_X_test_imp[name]  = pickle.load(open("Imputation_data/tmp/IR_X_test_imp_%s.pkl"%name , 'rb'))
    CIR_test_imp[name]   = pickle.load(open("Imputation_data/tmp/CIR_test_imp_%s.pkl"%name  , 'rb'))

# upload pickled data
MR = pickle.load(open("Transformed_data/MR_step2.pkl", 'rb'))
IR = pickle.load(open("Transformed_data/IR_step2.pkl", 'rb'))

# Country # nominal # OneHot encoding
MR_country_dummy = pd.get_dummies(MR, columns=['country'], prefix='Country')
IR_country_dummy = pd.get_dummies(IR, columns=['country'], prefix='Country')

# extraction of column names
MR_col_names = MR_country_dummy.columns.drop('hiv03')
IR_col_names = IR_country_dummy.columns.drop('hiv03')

#================================================================================================================================
# subsampling 9 countries out of 10
#================================================================================================================================

CMR_train = {}
CMR_test = {}
CIR_train = {}
CIR_test = {}
CMR_Y_train = {}
CMR_Y_test = {}
CIR_Y_train = {}
CIR_Y_test = {}


    
# imputation processing
def min_max_int(df, column, nb_cat=float('inf')):
    df[column] = round(df[column], 0)
    df[column][df[column] < 0] = 0
    df[column][df[column] > nb_cat - 1] = nb_cat - 1
    return df

def onehot(df, name):
    onehot = pd.DataFrame()
    for colonne in list(df.columns):
        if re.match(name, colonne):
            for indice in df[colonne][~df[colonne].isin([0, 1])].index:
                onehot.loc[indice, colonne] = df.loc[indice, colonne]
    i = 0
    for indice in onehot.index:
        onehot.loc[indice, list(onehot.idxmax(axis=1))[i]] = 1
        i += 1
    onehot[onehot!=1] = 0
    df.update(onehot)
    return df

MR_X_train_proc =  {}
MR_X_test_proc =  {}
CMR_test_proc =  {}
IR_X_train_proc =  {}
IR_X_test_proc =  {}
CIR_test_proc =  {}

def MR_imp_process(df):
    min_max_int(df, 'Years lived in place of residence', df['Current age'] + 1)
    min_max_int(df, 'Highest educational level', 4)
    onehot(df, 'Religion')
    min_max_int(df, 'Total number of years of education')
    min_max_int(df, 'Usual resident or visitor', 2)
    onehot(df, 'Relationship to household head')
    min_max_int(df, 'Age of household head', 100 + 1)
    min_max_int(df, 'Literacy', 3)
    min_max_int(df, 'Frequency of reading newspaper or magazine', 4)
    min_max_int(df, 'Frequency of listening to radio', 4)
    min_max_int(df, 'Frequency of watching television', 4)
    min_max_int(df, 'Times away from home in last 12 months', 146 + 1)
    min_max_int(df, 'Sons at home')
    min_max_int(df, 'Daughters at home')
    min_max_int(df, 'Sons elsewhere')
    min_max_int(df, 'Daughters elsewhere')
    min_max_int(df, 'Sons who have died')
    min_max_int(df, 'Daughters who have died')
    onehot(df, 'Knowledge of ovulatory cycle')
    min_max_int(df, 'Number of women fathered children with')
    min_max_int(df, 'Knowledge of any contraceptive method', 4)
    min_max_int(df, 'Current contraceptive method', 2)
    min_max_int(df, 'Current contraceptive by method type', 4)
    min_max_int(df, 'Heard family planning on radio last few months', 2)
    min_max_int(df, 'Heard family planning on TV last few months', 2)
    min_max_int(df, 'Heard family planning in newspaper/magazine last few months', 2)
    min_max_int(df, 'Discussed Family Planning with health worker in last few months', 2)
    min_max_int(df, 'Contraception is woman\'s business, man should not worry', 2)
    min_max_int(df, 'Women who use contraception become promiscuous', 2)
    min_max_int(df, 'Number of injections in last 12 months', 146 + 1)
    min_max_int(df, 'Covered by health insurance', 2)
    min_max_int(df, 'Respondent circumcised', 2)
    min_max_int(df, 'Time since last sex (in days)')
    min_max_int(df, 'Age at first sex (imputed)', df['Current age'] + 1)
    min_max_int(df, 'Recent sexual activity', 2)
    min_max_int(df, 'Fertility preference', 4)
    min_max_int(df, 'Ideal number of children')
    min_max_int(df, 'Ideal number of boys')
    min_max_int(df, 'Ideal number of girls')
    min_max_int(df, 'Ideal number of either sex')
    min_max_int(df, 'Wife justified refusing sex: husband has other women', 2)
    min_max_int(df, 'Currently working', 2)
    onehot(df, 'Occupation')
    min_max_int(df, 'Respondent worked in last 7 days', 3)
    min_max_int(df, 'Employment all year/seasonal', 4)
    min_max_int(df, 'Type of earnings from respondent\'s work', 4)
    min_max_int(df, 'Owns a house alone or jointly', 4)
    min_max_int(df, 'Owns land alone or jointly', 4)
    min_max_int(df, 'Condom used during last sex with most recent partner', 2)
    min_max_int(df, 'Had any STI in last 12 months', 2)
    min_max_int(df, 'Had genital sore/ulcer in last 12 months', 2)
    min_max_int(df, 'Had genital discharge in last 12 months', 2)
    min_max_int(df, 'Number of sex partners, including spouse, in last 12 months')
    min_max_int(df, 'Relationship with most recent sex partner', 6)
    min_max_int(df, 'Know a place to get HIV test', 2)
    min_max_int(df, 'Heard about other STIs', 2)
    min_max_int(df, 'Have ever paid anyone in exchange for sex', 2)
    min_max_int(df, 'Paid for sex in last 12 months', 2)
    min_max_int(df, 'Wife justified asking husband to use condom if he has STI', 2)
    min_max_int(df, 'Drugs to avoid HIV transmission to baby during pregnancy', 2)    
    min_max_int(df, 'Would buy vegetables from vendor with HIV', 2)
    min_max_int(df, 'Age of most recent partner', 100 + 1)
    df['Age of most recent partner'][df['Age of most recent partner'] < 10] = 10
    min_max_int(df, 'Total lifetime number of sex partners', 146 + 1)
    df['Total lifetime number of sex partners'][df['Total lifetime number of sex partners'] == 0] = 1
    min_max_int(df, 'Times in last 12 months had sex with most recent partner', 146 + 1)
    df['Times in last 12 months had sex with most recent partner'][df['Times in last 12 months had sex with most recent partner'] == 0] = 1
    min_max_int(df, 'Cluster altitude in meters')
    min_max_int(df, 'Reduce risk of getting HIV', 2)
    min_max_int(df, 'Ways of transmission from mother to child', 2)
    return df

print('starting MR imp process')
for name in list(country_list):
    print(f'loading {name}...')
    MR_X_train_proc[name] = MR_imp_process(MR_X_train_imp[name])
    MR_X_test_proc[name] = MR_imp_process(MR_X_test_imp[name])
    CMR_test_proc[name] = MR_imp_process(CMR_test_imp[name])

def IR_imp_process(df):
    min_max_int(df, 'Cluster altitude in meters')
    min_max_int(df, 'Years lived in place of residence', df['Current age'] + 1)
    min_max_int(df, 'Highest educational level', 4)
    min_max_int(df, 'Time to get to water source')
    min_max_int(df, 'Household has: electricity', 2)
    min_max_int(df, 'Household has: radio', 2)
    min_max_int(df, 'Household has: television', 2)
    min_max_int(df, 'Household has: refrigerator', 2)
    min_max_int(df, 'Household has: bicycle', 2)
    min_max_int(df, 'Household has: motorcycle/scooter', 2)
    min_max_int(df, 'Household has: car/truck', 2)
    onehot(df, 'Religion')
    min_max_int(df, 'Total number of years of education')
    min_max_int(df, 'Usual resident or visitor', 2)
    onehot(df, 'Relationship to household head')
    min_max_int(df, 'Age of household head', 100 + 1)
    min_max_int(df, 'Household has: telephone (land-line)', 2)
    min_max_int(df, 'Literacy', 3)
    min_max_int(df, 'Frequency of reading newspaper or magazine', 4)
    min_max_int(df, 'Frequency of listening to radio', 4)
    min_max_int(df, 'Frequency of watching television', 4)
    min_max_int(df, 'Toilet facilities shared with other households', 2)
    min_max_int(df, 'Times away from home in last 12 months', 146 + 1)
    min_max_int(df, 'Type of mosquito bed net(s) slept under last night', 4)
    min_max_int(df, 'Sons at home')
    min_max_int(df, 'Daughters at home')
    min_max_int(df, 'Sons elsewhere')
    min_max_int(df, 'Daughters elsewhere')
    min_max_int(df, 'Sons who have died')
    min_max_int(df, 'Daughters who have died')
    min_max_int(df, 'Births in last five years')
    min_max_int(df, 'Births in past year')
    min_max_int(df, 'Births in month of interview')
    min_max_int(df, 'Currently pregnant', 2)
    min_max_int(df, 'Menstruated in last six weeks', 2)
    onehot(df, 'Knowledge of ovulatory cycle')
    min_max_int(df, 'Entries in birth history')
    min_max_int(df, 'Ever had a terminated pregnancy', 2)
    min_max_int(df, 'Index last child prior to maternity-health (calendar)')    
    min_max_int(df, 'Births in last three years')
    min_max_int(df, 'Knowledge of any contraceptive method', 4)
    min_max_int(df, 'Ever used anything or tried to delay or avoid getting pregnant', 3)
    min_max_int(df, 'Current contraceptive method', 2)
    min_max_int(df, 'Current contraceptive by method type', 4)
    min_max_int(df, 'Pattern of contraceptive use', 4)
    min_max_int(df, 'Contraceptive use and intention', 4)
    min_max_int(df, 'Heard family planning on radio last few months', 2)
    min_max_int(df, 'Heard family planning on TV last few months', 2)
    min_max_int(df, 'Heard family planning in newspaper/magazine last few months', 2)
    min_max_int(df, 'Visited by fieldworker in last 12 months', 2)
    min_max_int(df, 'Visited health facility last 12 months', 2)
    min_max_int(df, 'Currently breastfeeding', 2)
    min_max_int(df, 'Currently amenorrheic', 2)
    min_max_int(df, 'Currently abstaining', 2)
    min_max_int(df, 'Heard of oral rehydration', 3)
    min_max_int(df, 'Rohrer\'s index')
    df['Rohrer\'s index'][df['Rohrer\'s index'] < 726] = 726
    min_max_int(df, 'Have mosquito bed net for sleeping', 2)
    min_max_int(df, 'Respondent slept under mosquito bed net', 2)
    min_max_int(df, 'Does not use cigarettes and tobacco', 2)
    min_max_int(df, 'Getting medical help for self: getting permission to go', 2)
    min_max_int(df, 'Getting medical help for self: getting money needed for treatment', 2)
    min_max_int(df, 'Getting medical help for self: distance to health facility', 2)
    min_max_int(df, 'Getting medical help for self: not wanting to go alone', 2)
    min_max_int(df, 'Number of injections in last 12 months', 146 + 1)
    min_max_int(df, 'Covered by health insurance', 2)
    min_max_int(df, 'Number of unions', 2)
    min_max_int(df, 'Age at first cohabitation', df['Current age'] + 1)
    min_max_int(df, 'Years since first cohabitation', df['Current age'] + 1)
    min_max_int(df, 'Time since last sex (in days)')
    min_max_int(df, 'Age at first sex (imputed)', df['Current age'] + 1)
    min_max_int(df, 'Recent sexual activity', 2)
    min_max_int(df, 'Fertility preference', 4)
    min_max_int(df, 'Ideal number of children')
    min_max_int(df, 'Fecund (definition 3)', 2)
    onehot(df, 'Unmet need for contraception')
    min_max_int(df, 'Ideal number of boys')
    min_max_int(df, 'Ideal number of girls')
    min_max_int(df, 'Ideal number of either sex')
    min_max_int(df, 'Wife justified refusing sex: husband has other women', 2)
    min_max_int(df, 'Currently working', 2)
    onehot(df, 'Occupation')
    min_max_int(df, 'Respondent worked in last 7 days', 4)
    min_max_int(df, 'Owns a house alone or jointly', 4)
    min_max_int(df, 'Owns land alone or jointly', 4)
    min_max_int(df, 'Ever heard of a Sexually Transmitted Infection (STI)', 2)
    min_max_int(df, 'Condom used during last sex with most recent partner', 2)
    min_max_int(df, 'Had any STI in last 12 months', 2)
    min_max_int(df, 'Had genital sore/ulcer in last 12 months', 2)
    min_max_int(df, 'Had genital discharge in last 12 months', 2)
    min_max_int(df, 'Number of sex partners, including spouse, in last 12 months')
    min_max_int(df, 'Relationship with most recent sex partner', 6)
    min_max_int(df, 'Ever been tested for HIV', 2)
    min_max_int(df, 'Know a place to get HIV test', 2)
    min_max_int(df, 'Heard about other STIs', 2)
    min_max_int(df, 'Wife justified asking husband to use condom if he has STI', 2)
    min_max_int(df, 'Drugs to avoid HIV transmission to baby during pregnancy', 2)   
    min_max_int(df, 'Would buy vegetables from vendor with HIV', 2)
    min_max_int(df, 'Age of most recent partner', 100 + 1)
    df['Age of most recent partner'][df['Age of most recent partner'] < 10] = 10
    min_max_int(df, 'Total lifetime number of sex partners', 146 + 1)
    df['Total lifetime number of sex partners'][df['Total lifetime number of sex partners'] == 0] = 1
    min_max_int(df, 'Times in last 12 months had sex with most recent partner', 146 + 1)
    df['Times in last 12 months had sex with most recent partner'][df['Times in last 12 months had sex with most recent partner'] == 0] = 1
    min_max_int(df, 'Beating justified', 2)
    min_max_int(df, 'Reduce risk of getting HIV', 2)
    min_max_int(df, 'Ways of transmission from mother to child', 2)
    min_max_int(df, 'Presence of other people for \'Wife beating justified\' questions', 2)
    min_max_int(df, 'Presence of other people during the sexual activity section of the interview', 2)
    return df

print('starting IR imp process')
for name in list(country_list):
    print(f'loading {name}...')
    IR_X_train_proc[name] = IR_imp_process(IR_X_train_imp[name])
    IR_X_test_proc[name] = IR_imp_process(IR_X_test_imp[name])
    CIR_test_proc[name] = IR_imp_process(CIR_test_imp[name])
    
MR_X_train_ready = {}
MR_X_test_ready = {}
CMR_test_ready = {}
IR_X_train_ready = {}
IR_X_test_ready = {}
CIR_test_ready = {}

# standardization of the data
print('standardizing data')
for name in list(country_list):
    print(f'loading {name}...')
    MR_scaler = StandardScaler(with_mean=False)
    MR_X_train_ready[name] = round(pd.DataFrame(MR_scaler.fit_transform(MR_X_train_proc[name]), columns=MR_col_names), 2)
    MR_X_test_ready[name] = round(pd.DataFrame(MR_scaler.transform(MR_X_test_proc[name]), columns=MR_col_names), 2)
    CMR_test_ready[name] = round(pd.DataFrame(MR_scaler.transform(CMR_test_proc[name]), columns=MR_col_names), 2)
    MR_var = MR_scaler.var_
    IR_scaler = StandardScaler(with_mean=False)
    IR_X_train_ready[name] = round(pd.DataFrame(IR_scaler.fit_transform(IR_X_train_proc[name]), columns=IR_col_names), 2)
    IR_X_test_ready[name] = round(pd.DataFrame(IR_scaler.transform(IR_X_test_proc[name]), columns=IR_col_names), 2)
    CIR_test_ready[name] = round(pd.DataFrame(IR_scaler.transform(CIR_test_proc[name]), columns=IR_col_names), 2)
    IR_var = IR_scaler.var_
    
print('saving data')
# pickling data
f = open("Imputation_data/MR_X_train_imp.pkl", 'wb')
pickle.dump(MR_X_train_imp, f)
f = open("Imputation_data/MR_X_test_imp.pkl", 'wb')
pickle.dump(MR_X_test_imp, f)
f = open("Imputation_data/CMR_test_imp.pkl", 'wb')
pickle.dump(CMR_test_imp, f)
f = open("Imputation_data/IR_X_train_imp.pkl", 'wb')
pickle.dump(IR_X_train_imp, f)
f = open("Imputation_data/IR_X_test_imp.pkl", 'wb')
pickle.dump(IR_X_test_imp, f)
f = open("Imputation_data/CIR_test_imp.pkl", 'wb')
pickle.dump(CIR_test_imp, f)
f = open("Imputation_data/MR_X_train_comp.pkl", 'wb')
pickle.dump(MR_X_train_comp, f)
f = open("Imputation_data/MR_X_test_comp.pkl", 'wb')
pickle.dump(MR_X_test_comp, f)
f = open("Imputation_data/CMR_test_comp.pkl", 'wb')
pickle.dump(CMR_test_comp, f)
f = open("Imputation_data/IR_X_train_comp.pkl", 'wb')
pickle.dump(IR_X_train_comp, f)
f = open("Imputation_data/IR_X_test_comp.pkl", 'wb')
pickle.dump(IR_X_test_comp, f)
f = open("Imputation_data/CIR_test_comp.pkl", 'wb')
pickle.dump(CIR_test_comp, f)
f = open("Imputation_data/MR_X_train_proc.pkl", 'wb')
pickle.dump(MR_X_train_proc, f)
f = open("Imputation_data/MR_X_test_proc.pkl", 'wb')
pickle.dump(MR_X_test_proc, f)
f = open("Imputation_data/CMR_test_proc.pkl", 'wb')
pickle.dump(CMR_test_proc, f)
f = open("Imputation_data/IR_X_train_proc.pkl", 'wb')
pickle.dump(IR_X_train_proc, f)
f = open("Imputation_data/IR_X_test_proc.pkl", 'wb')
pickle.dump(IR_X_test_proc, f)
f = open("Imputation_data/CIR_test_proc.pkl", 'wb')
pickle.dump(CIR_test_proc, f)
f = open("Train_samples/MR_X_train_ready.pkl", 'wb')
pickle.dump(MR_X_train_ready, f)
f = open("Test_samples/MR_X_test_ready.pkl", 'wb')
pickle.dump(MR_X_test_ready, f)
f = open("Left_one_out_samples/CMR_test_ready.pkl", 'wb')
pickle.dump(CMR_test_ready, f)
f = open("Train_samples/IR_X_train_ready.pkl", 'wb')
pickle.dump(IR_X_train_ready, f)
f = open("Test_samples/IR_X_test_ready.pkl", 'wb')
pickle.dump(IR_X_test_ready, f)
f = open("Left_one_out_samples/CIR_test_ready.pkl", 'wb')
pickle.dump(CIR_test_ready, f)
f = open("Imputation_data/MR_var.pkl", 'wb')
pickle.dump(MR_var, f)
f = open("Imputation_data/IR_var.pkl", 'wb')
pickle.dump(IR_var, f)
f.close()
